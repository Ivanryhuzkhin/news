//
//  Date+Extension.swift
//  News
//
//  Created by Иван Рыжухин on 06.04.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import Foundation

extension Date {
    // преобразовывает Date в String с заданным форматов
    func converToString(withFormat format:String, andLocale locale: String = "en_US_POSIX") -> String{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: locale)
        formatter.dateFormat = format
        return formatter.string(from: self)
        
    }
}
