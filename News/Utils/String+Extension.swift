//
//  String+Extension.swift
//  News
//
//  Created by Иван Рыжухин on 06.04.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import Foundation

extension String {
    // преобразовывает String в Date с заданным форматов
    // возвращает опционал
   func convertToDate(withFormat format: String, andLocale locale: String = "en_US_POSIX") -> Date? {
       let formatter = DateFormatter()
       formatter.locale = Locale(identifier: locale)
       formatter.dateFormat = format
           
       return formatter.date(from: self)
   }
}
