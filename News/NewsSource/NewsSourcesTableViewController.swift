//
//  NewsSourcesTableViewController.swift
//  News
//
//  Created by Иван Рыжухин on 06.04.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import UIKit

protocol NewsSourcesTableViewControllerDelegate {
    func updateSource(at index: Int)
}

class NewsSourcesTableViewController: UITableViewController {

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(model:[NewsSource], index: Int) {
        self.newsSource = model
        self.selectedSourceIndex = index
        super.init(nibName: nil, bundle: nil)
    }
    
    // Список добавленных источников новостей
    fileprivate var newsSource: [NewsSource]
    // Индекс выбранного итсочника
    fileprivate var selectedSourceIndex:Int
    var delegate: NewsSourcesTableViewControllerDelegate?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(NewsSourceTableViewCell.self, forCellReuseIdentifier: "Cell")
        prepareHeaderView()
    }

    
    // зададим заголовок в таблице чтобы добавить кнопку отмена -> выход из VC
    func prepareHeaderView() {
        let header = UIView()
        header.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.tableHeaderView =  header

        header.widthAnchor.constraint(equalTo: self.tableView.widthAnchor).isActive = true
        header.topAnchor.constraint(equalTo: self.tableView.topAnchor).isActive = true


        
        let button = UIButton()
        button.setTitle("Отмена", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.addTarget(self, action: #selector(onCancelPress), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        header.addSubview(button)
        
        
        button.topAnchor.constraint(equalTo: header.topAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: header.bottomAnchor).isActive = true
        button.leadingAnchor.constraint(equalTo: header.leadingAnchor, constant: 16 ).isActive = true
        button.trailingAnchor.constraint(lessThanOrEqualTo: header.trailingAnchor, constant: -16).isActive = true

        
        self.tableView.tableHeaderView =  header
        
        self.tableView.tableHeaderView?.layoutIfNeeded()
    }
    
    @objc func onCancelPress() {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newsSource.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? NewsSourceTableViewCell else {return UITableViewCell()}
        
        let dataSource = newsSource[indexPath.row]
        cell.title = dataSource.title
        cell.subtitle = dataSource.source
        if selectedSourceIndex == indexPath.row {
            cell.accessoryType = .checkmark
        }
        
        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate?.updateSource(at: indexPath.row)
        self.dismiss(animated: true, completion: nil)
    }
    

}
