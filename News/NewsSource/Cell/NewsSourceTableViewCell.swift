//
//  NewsSourceTableViewCell.swift
//  News
//
//  Created by Иван Рыжухин on 06.04.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import Foundation
import UIKit

class NewsSourceTableViewCell: UITableViewCell {
    
    private var lTitle:UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        lbl.font = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
        return lbl
    }()
    private var lSubtitle: UILabel = {
        let lbl =  UILabel()
        lbl.numberOfLines = 1
        lbl.lineBreakMode = .byTruncatingTail
        lbl.font = UIFont.systemFont(ofSize: 13.0)
        lbl.textColor = .systemGray
        lbl.textAlignment = .left
        return lbl
    }()
    
    var title:String = "" {
        didSet {
            lTitle.text = title
        }
    }
    
    var subtitle:String = "" {
        didSet {
            lSubtitle.text = subtitle
        }
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.prepareTitleLabel()
        self.prepareSubtitleLabel()

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
//        wasRead = false
    }
    
    func prepareTitleLabel() {
        self.lTitle.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.lTitle)
        
        lTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16.0).isActive = true
        lTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -32.0).isActive = true
        lTitle.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0).isActive = true
        lTitle.setContentHuggingPriority(.defaultHigh, for: .vertical)
        lTitle.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
    }
    
    func prepareSubtitleLabel() {
        self.lSubtitle.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.lSubtitle)
        
        lSubtitle.topAnchor.constraint(equalTo: self.lTitle.bottomAnchor, constant: 5.0).isActive = true
        lSubtitle.trailingAnchor.constraint(greaterThanOrEqualTo: self.trailingAnchor, constant: 16).isActive = true
        lSubtitle.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0).isActive = true
        lSubtitle.leadingAnchor.constraint(equalTo: lTitle.leadingAnchor).isActive = true
        
        lSubtitle.setContentHuggingPriority(.defaultLow, for: .vertical)
        lSubtitle.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
    }
}

