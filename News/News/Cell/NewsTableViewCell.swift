//
//  NewsTableViewCell.swift
//  News
//
//  Created by Иван Рыжухин on 06.04.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import Foundation
import UIKit


class NewsTableViewCell: UITableViewCell {
    
    private var lTitle:UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        lbl.font = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
        return lbl
    }()
    private var lDate: UILabel = {
        let lbl =  UILabel()
        lbl.numberOfLines = 1
        lbl.lineBreakMode = .byTruncatingTail
        lbl.font = UIFont.systemFont(ofSize: 13.0)
        lbl.textColor = .systemGray
        lbl.textAlignment = .left
        return lbl
    }()
    
    
    var date:Date? {
        didSet {
            dateString = date?.converToString(withFormat: "d MMM, hh:mm ", andLocale: "ru_RU") ?? ""
        }
    }
    
    var dateString: String = "" {
        didSet {
            lDate.text = dateString
        }
    }
    
    var title:String = "" {
        didSet {
            lTitle.text = title
        }
    }
    
    var wasRead:Bool = false {
        didSet {
            self.lTitle.alpha = wasRead ? 0.3 : 1
            self.lDate.alpha = wasRead ? 0.3 : 1
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.prepareTitleLabel()
        self.prepareDateLabel()

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
//        wasRead = false
    }
    
    func prepareTitleLabel() {
        self.lTitle.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.lTitle)
        
        lTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16.0).isActive = true
        lTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -32.0).isActive = true
        lTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0).isActive = true
        lTitle.setContentHuggingPriority(.defaultHigh, for: .vertical)
        lTitle.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
    }
    
    func prepareDateLabel() {
        self.lDate.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.lDate)
        
        lDate.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0).isActive = true
        lDate.trailingAnchor.constraint(greaterThanOrEqualTo: self.trailingAnchor, constant: 16).isActive = true
        lDate.bottomAnchor.constraint(equalTo: lTitle.topAnchor, constant: -5.0).isActive = true
        lDate.leadingAnchor.constraint(equalTo: lTitle.leadingAnchor).isActive = true
        
        lDate.setContentHuggingPriority(.defaultLow, for: .vertical)
        lDate.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
    }
}
