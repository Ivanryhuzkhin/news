//
//  NewsTableViewController.swift
//  News
//
//  Created by Иван Рыжухин on 05.04.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import UIKit

class NewsTableViewController: UITableViewController, NewsSourcesTableViewControllerDelegate {

    
    // список новостей
    var news: [News] = []
    
    // разбираемый в данный момент элемент
    fileprivate var tempElement: String = ""
    fileprivate var tempItem: News?
    
    // список новостей кооторые были просмотрены
    // считаем что вероятность совпадание дат публикации кране низка и дату публикации можно использовать как id
    // выбранной новости за неимением id в rss
    fileprivate var readedNews = Set<TimeInterval>()
    
    var sourceUrls = [NewsSource]()
    var selectedSourceIndex:Int = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: "Cell")
        
        let initialSource = NewsSource(title: "Финам", source: "https://www.finam.ru/net/analysis/conews/rsspoint")
        let sourceURL = URL(string: initialSource.source)
        if let sourceURL = sourceURL {
            sourceUrls.append(initialSource)
            updateParser(url: sourceURL)
        }
        
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = .systemGray
        refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        // настроим панель управления в навигейшен баре
        setupNavigationBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    
    // создадим кнопку для источников новостеей
    // создадим кнопку для ддобавления источника
    // зададим заголовок экрана
    func setupNavigationBar() {
        self.navigationController?.navigationBar.topItem?.title =  sourceUrls[selectedSourceIndex].title + " новости"
        
        let rightButton = UIBarButtonItem(image: UIImage(systemName: "plus"), style: .plain, target: self, action: #selector(onAddSourcePress))
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = rightButton
        
        let leftButton = UIBarButtonItem(title: "Источники", style: .plain, target: self, action: #selector(onShowSourcesPress))
        
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = leftButton
    }
    
    // обработчик нажатия на + в навигейшен баре
    // добавляет источник для новостей
    @objc func onAddSourcePress () {
        let alertController = UIAlertController(title: "Ввеедите источник для новостей", message: "Источник в формате RSS", preferredStyle: .alert)
        var tfTitle: UITextField = UITextField()
        var tfURL: UITextField = UITextField()
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Заголовок"
            tfTitle = textField
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "URL"
            tfURL = textField
        }
        
        let okAction = UIAlertAction(title: "Добавить", style: .default) { (alertAcction) in
            let titel = tfTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            let url = tfURL.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            if let title = titel, title.isEmpty == false, let url = url, url.isEmpty == false {
                let newSource = NewsSource(title: title, source: url)
                self.sourceUrls.append(newSource)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController,animated: true)
    }
    // обработчик нажатия на Править в навигейшен баре
    // необходимо для выбора новостей
    @objc func onShowSourcesPress () {
        let vc = NewsSourcesTableViewController.init(model: sourceUrls, index: selectedSourceIndex)
        vc.delegate = self
        self.present(vc, animated:  true)
    }
    
    func updateParser(url: URL?) {
        if let sourceURL = url {
            let parser = XMLParser(contentsOf: sourceURL)
            parser?.delegate = self
            parser?.parse()
        }
    }
    
    // обновление контента
    @objc func  refresh (sender:UIRefreshControl) {
        // обнулим имеющийся датасоурс и загрузим новые данные из rss
        news = []
        updateParser(url: URL(string: self.sourceUrls[selectedSourceIndex].source))
    }
    
    // изменим выбранный источник данных
    func updateSource(at index: Int) {
        selectedSourceIndex =  index
        news = []
        self.navigationController?.navigationBar.topItem?.title = sourceUrls[selectedSourceIndex].title + " новости"
        updateParser(url: URL(string: self.sourceUrls[selectedSourceIndex].source))
    }
    
    
// MARK -- Table view
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? NewsTableViewCell else {return  UITableViewCell()}
        cell.separatorInset = .init(top: 0, left: 16.0, bottom: 0, right: 0)
        cell.accessoryType = .disclosureIndicator
        let dataSource = news[indexPath.row]
        cell.title = dataSource.title
        cell.date = dataSource.date
        if let date = dataSource.date{
            cell.wasRead = readedNews.contains(date.timeIntervalSince1970)
        }
        else {
            cell.wasRead = false
        }
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // покажем текст новости
        let model = news[indexPath.row]
        let vc = NewsInfoViewController(model: model)
        if let cell = tableView.cellForRow(at: indexPath) as? NewsTableViewCell {
            cell.wasRead = true
        }
        if let date = model.date {
            readedNews.update(with: date.timeIntervalSince1970)
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension NewsTableViewController: XMLParserDelegate{
    
    func parserDidStartDocument(_ parser: XMLParser) {
        print("begin")
    }
    func parserDidEndDocument(_ parser: XMLParser) {
        print("end")
        self.refreshControl?.endRefreshing()
        tableView.reloadData()
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        // если встретилась ошибка при парсинге хмл выведем ее в лог
        print("parse error \(parseError.localizedDescription)")
    }
    
    // начало парсинга нового элемента xml
    // создадим модель новости
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        tempElement = elementName
        if elementName == "item" {
            tempItem = News(title: "", description: "", pubDate: "")
        }
    }
    
    // окончание парсинга элемента xml
    // добавим элемент item в массив хранящий новости
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            if let item = tempItem {
                news.append(item)
            }
            tempItem = nil
        }
        
    }
    
    // распарсим xml
    // получим  title, description, description
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        if let item = tempItem, !string.contains("\n") {
            if tempElement == "title" {
                tempItem?.title = item.title+string
            } else if tempElement == "description"{
                tempItem?.description = item.description+string
            } else if tempElement == "pubDate"{
                tempItem?.pubDate = item.pubDate+string
            }
        }
    }
}
