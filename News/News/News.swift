//
//  News.swift
//  News
//
//  Created by Иван Рыжухин on 05.04.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import Foundation
// модель для хранения информации о новости
// title, date, description поля из RSS формата.
// в соответствии с форматом не опциональные
// wasReaded - признак просмотра новости. По умолчанию false т.к. не просмотрена.
struct News: Codable {
    var title:String
    var description: String
    var pubDate: String {
        didSet {
            date = pubDate.convertToDate(withFormat: "E, d MMM yyyy HH:mm:ss Z")
        }
    }
    var date:Date?
    var wasReaded:Bool = false
}

struct NewsSource: Codable {
    var title:String
    var source: String
}
