//
//  NewsInfoViewController.swift
//  News
//
//  Created by Иван Рыжухин on 06.04.2020.
//  Copyright © 2020 Ivan Ryzhukhin. All rights reserved.
//

import UIKit
import WebKit

class NewsInfoViewController: UIViewController, WKNavigationDelegate {
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(model:News) {
        self.model = model
        self.dateString = ""
        super.init(nibName: nil, bundle: nil)
    }
    
    var model: News
    private var date:Date? {
        didSet {
            dateString = date?.converToString(withFormat: "d MMMM hh:mm ", andLocale: "ru") ?? ""
        }
    }
    private var dateString:String
    
    private var scrollView:UIScrollView = {
        let sv = UIScrollView()
        sv.showsHorizontalScrollIndicator = false
        return sv
    }()
    
    private var stackView:UIStackView = {
       let sv = UIStackView()
        sv.spacing = 12
        sv.axis = .vertical
        return sv
    }()
    
    private var tvTitle:UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.font = UIFont.systemFont(ofSize: 22.0, weight: .semibold)
        textView.textAlignment = .left
        return textView
    }()
    
    private var tvDate: UITextView = {
        let textView =  UITextView()
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.font = UIFont.systemFont(ofSize: 13.0)
        textView.textAlignment = .left
        textView.textColor = .systemGray
        return textView
    }()
    
    private var wvDescription: WKWebView = {
        let webView =  WKWebView()
        
    return webView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.date = model.date
        
        addWebView()
        addWebViewData(title: model.title, date: dateString, description: model.description)
    
    }
    // добавление веб вью в ui
    func addWebView() {
        wvDescription.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(wvDescription)
        
        wvDescription.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        wvDescription.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        wvDescription.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        wvDescription.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        
        
    }
    // настройка стилеей для веб вьбю и вставка данных
    func addWebViewData(title: String, date: String, description: String) {
        var resultHtmlStr: String =
                    """
                        <head>
                        <style>
                            html, body {
                                margin: 0; padding: 0;
                            }

                            a {
                                text-decoration:none;
                                color: #E60F2C !important;
                            }
                            
                            .title-div {
                                font-family: -apple-system, BlinkMacSystemFont, sans-serif;
                                font-size:70px;
                                font-weight:bold;
                                margin: 21px 42px 12px 42px;
                            }
                            .date-div {
                                font-family: -apple-system, BlinkMacSystemFont, sans-serif;
                                font-size:39px;
                                font-weight:regular;
                                color:rgb(0,0,0);
                                opacity:0.4;
                                margin: 0px 0px 28px 42px;
                            }
                            
                            p, ul, li {
                                font-family: -apple-system, BlinkMacSystemFont, sans-serif;
                                margin-left: 42px;
                                margin-right: 42px;
                                font-size:52px;
                            }


                            img {
                                position: static;
                                margin-left: -42px !important;
                                width:110% !important;
                            }

                            iframe {
                                position: static;
                                width:100% !important;
                            }
                            
                            .content-div {
                               font-family: -apple-system, BlinkMacSystemFont, sans-serif;
                               margin-left: 42px;
                               margin-right: 42px;
                               font-size:52px;
                            }
                    }
                        </style>
                    </head>
                    <body>
                    """
                    
            resultHtmlStr += "<div class=\"title-div\">\(title)</div>"
            

            resultHtmlStr += "<div class=\"date-div\">\(date)</div>"
        
            resultHtmlStr += "<div class=\"content-div\">\(description)</div>"
        
        resultHtmlStr += """
        """
        resultHtmlStr += "</body>"
        self.wvDescription.loadHTMLString(resultHtmlStr, baseURL: nil)
    }
    
    // обработаем нажатие на ссылку
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated  {
            if let url = navigationAction.request.url, UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
                print(url)
                print("Redirected to browser. No need to open it locally")
                decisionHandler(.cancel)
            } else {
                print("Open it locally")
                decisionHandler(.allow)
            }
        } else {
            print("not a user click")
            decisionHandler(.allow)
        }
    }
    

}
